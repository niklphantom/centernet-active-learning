from matplotlib import pyplot as plt

num_images = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]
map_al = [0.12653934229522784,
        0.29999473184756387,
        0.5839909574622731,
        0.7476971334696562,
        0.7265786070335652,
        0.8811262255985551,
        0.9059363578899271,
        0.9213504385793245,
        0.9402478619451868,
        0.9390956447026824]
map_rs = [0.12119311727098889,
        0.22030463937415562,
        0.6642500378886594,
        0.8307907273292076,
        0.8392776389789365,
        0.9294676448924779,
        0.9265816630793726,
        0.955127253478694,
        0.9496840363017097,
        0.954221198974417]

map_al_reverse = [
        0.12107535230047486,
        0.17858247236168517,
        0.6684113226309373,
        0.8952935623432913,
        0.8808034448152353,
        0.9538022542802528,
        0.9454165621514068,
        0.9550835439674515,
        0.9606283391728601,
        0.9609960570072779
]

plt.plot(num_images, map_al, label='Active learning', marker='o')
plt.plot(num_images, map_rs, label='Random sampling', marker='o')
plt.plot(num_images, map_al_reverse, label='Active learning reverse', marker='o')
plt.xlabel("Number of images")
plt.ylabel("mAP IoU 0.5")
plt.legend()
plt.grid()
# plt.title("9 cycles, +500 images, 5 epoch in cycle, no augmentations")
plt.title("10 cycles, +500 images, 14 epoch in cycle, augmentations")
# plt.show()
plt.savefig("al_plot.png")