from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# from utils.utils import save_anno_xml

import src._init_paths

import  numpy as np
import os
import cv2

from opts import opts
from detectors.detector_factory import detector_factory
from utils.data_pipeline import get_annotations_lists
from src.visualization import predict_and_visualize
from compute_metrics.plot_metrics import Stat, plot_graphics
from utils_cn.create_config import load_config
from datasets.dataset_factory import get_dataset
from utils.utils import save_anno_xml  # this is one for all detection architectures
import global_vars

color_list = [ (255,255,255), (255, 10,10), (0,0,255), (0,255,0), (13, 199, 255), (10, 230, 250), (5, 131, 250), (255, 13, 250)]   # TODO make it able to have arbitrary number of colors
# color_list = [ (255,255,255), (64, 225, 223), (25,232,75),  (0,0,255), (0,255,0), (13, 199, 255), (10, 230, 250), (5, 131, 250)]
for i in range(4):  # hahah watch this
    color_list.extend(color_list)



def CenterNet_predict_from_folder(config ="", weights="", image_dir="", working_dir="", save_predicted=True, visualize=False, opt=None ):
    if isinstance(config, str):  # in case you want to pass it as a path to config, my dear
        cfg = load_config(config)
    else:
        cfg = config


    detector = weights
    if isinstance(weights, str):
        cfg["load_model"] = weights
        opt = opts().parse(dict_args=cfg)
        Dataset = get_dataset(opt.dataset, opt.task)
        opt = opts().update_dataset_info_and_set_heads(opt, Dataset)
        Detector = detector_factory[opt.task]
        detector = Detector(opt)
    elif opt is None:
        opt = opts().parse(dict_args=cfg)
        Dataset = get_dataset(opt.dataset, opt.task)
        opt = opts().update_dataset_info_and_set_heads(opt, Dataset)

    os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str

    image_ext = ["jpg", "JPG", "jpeg", "png", "gif", "tiff", "bmp", "PNG"]
    if isinstance(image_dir, list):
        pass
    else:
        if os.path.isdir(image_dir):
            image_names = []
            ls = os.listdir(image_dir)
            for file_name in sorted(ls):
                ext = file_name[file_name.rfind('.') + 1:].lower()
                if ext in image_ext:
                    image_names.append(os.path.join(image_dir, file_name))
        img_names = image_names

    if working_dir and visualize:
        if not os.path.exists(working_dir + "images/"):
            os.mkdir(working_dir + "images/")

    if working_dir and save_predicted:
        if not os.path.exists(working_dir + "pred/"):
            os.mkdir(working_dir + "pred/")

    predictions_list = []
    for i, img_name in enumerate(img_names):  # TODO add batch size support
        if global_vars.global_stop_now:
            break
        if type(img_name) is np.ndarray:
            img = img_name
        else:
            img = cv2.imread(img_name)  # to get shape and save img with boxes, neural networks reads this image again in another function
        img_w_orig = img.shape[1]
        img_h_orig = img.shape[0]
        img_w = cfg["input_res"][0]
        img_h = cfg["input_res"][1]
        img = cv2.resize(img, (img_w, img_h))
        ret = detector.run(img_name)
        filtered_preds = filter_results(ret["results"], opt.classes, opt.vis_thresh)
        current_prediction = [img_name, filtered_preds]
        predictions_list.append(current_prediction)

        if working_dir and save_predicted:
            file_name = os.path.split(img_name)[1]
            img_end = file_name[file_name.rfind("." ) +1:]
            file_name = file_name[:file_name.rfind(".")]

            xml_boxes = []
            # image_depth = cfg.img_depth
            image_depth = 3
            # img_end = 'jpg'
            for j, det_box in enumerate(filtered_preds):
                #  make it conf, class, x1, y1, x2, y2
                xml_box = [float(det_box[5]), cfg["classes"][int(det_box[4])], int(det_box[0]), int(det_box[1]),
                           int(det_box[2]), int(det_box[3])]
                xml_boxes.append(xml_box)

            save_anno_xml(dir=working_dir + "pred/",
                          img_name=file_name,
                          img_format=img_end,
                          img_w=img_w,
                          img_h=img_h,
                          img_d=image_depth,
                          boxes=xml_boxes,
                          quiet=False,
                          minConf=0.1)

        if visualize:
            # file_name = os.path.split(img_name)[1]
            # file_name = file_name[:file_name.rfind(".")]

            for j, det_box in enumerate(filtered_preds):
                add_coco_bbox(img, det_box, int(det_box[4]), det_box[5], classes=opt.classes)
            cv2.imwrite(working_dir + "images/" + file_name + ".jpg", img)


def filter_results(results, classes,  vis_thresh):
    """
    Filters results of detection by a confidence threshold
    :param results: np.array of results sorted by classes in some CenterNet fashion
    :param classes: list of classes
    :param vis_thresh: confidence threshold
    :return: np.array with shape (n, 6) for n bboxes, where rows are x1, y1, x2, y2, cls_idx, cnf
    """
    filtered_preds = []
    for j in range(1, len(classes) + 1):
        for bbox in results[j]:
            if bbox[4] > vis_thresh:
                pred = [bbox[0], bbox[1], bbox[2], bbox[3], j-1, bbox[4]] # x1, y1, x2, y2. cls_idx, cnf
                # debugger.add_coco_bbox(bbox[:4], j - 1, bbox[4], img_id='ctdet')
                filtered_preds.append(pred)
    filtered_preds = np.array(filtered_preds)
    return  filtered_preds

def add_coco_bbox(img, bbox, cat_id,  conf=1, classes = [], show_txt=True, img_id='default', gt=False):
    bbox = np.array(bbox, dtype=np.int32)
    # cat = (int(cat) + 1) % 80
    # cat = int(cat)
    # print('cat', cat, self.names[cat])
    # c = self.colors[cat][0][0].tolist()
    # if self.theme == 'white':
    #     c = (255 - np.array(c)).tolist()
    c = color_list[cat_id + 1]  # since first color (255,255,255) is for ground truth
    if gt:
        c = (255,255,255)
        classes = ['Car', 'Van', 'Truck', 'Pedestrian', 'Cyclist', 'Tram', 'Misc', 'DontCare']
    txt = '{}{:.1f}'.format(classes[cat_id], conf)
    font = cv2.FONT_HERSHEY_SIMPLEX
    cat_size = cv2.getTextSize(txt, font, 0.5, 2)[0]
    cv2.rectangle(
        img, (bbox[0], bbox[1]), (bbox[2], bbox[3]), c, 2)
    if show_txt:
        cv2.rectangle(img,
                      (bbox[0], bbox[1] - cat_size[1] - 2),
                      (bbox[0] + cat_size[0], bbox[1] - 2), c, -1)
        cv2.putText(img, txt, (bbox[0], bbox[1] - 2),
                    font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
