import json
import numpy as np


def load_config(config_path):
    """
    Returns
    :param config_path: json file path
    :return: dict from loaded json file with some params changed back to np.arrays
    """

    with open(config_path, "r") as f:
        cfg = json.load(f)

    cfg["std"] = np.array(cfg["std"])
    cfg["mean"] = np.array(cfg["mean"])

    return cfg

def create_config(config_path):
    """
    Creates a config and saves it as json file at given path
    :param config_path: 
    :return: nothing 
    """
    classes = ['Car', 'Van', 'Truck',
                     'Pedestrian', 'Cyclist', 'Tram',
                     'Misc']
    class_to_idx = dict(zip(classes, range(len(classes))))  # TODO check if its correct i guess there in CenterNet should be idx + 1
    base_params = {
        "weights_name": None,
        "input_res" : (1280, 384, 3),  # (w,h,ch)
        "output_res": "idk_yet",
        "classes": classes,
        "class_to_idx": class_to_idx,
        "preprocessing": None,
        "dataset": "gazebo",  # dataset_name
        "val_loss": 0,
        "test_map": 0,
        "architecture": "CenterNet"
    }

    additional_params = {
    "task": "ctdet",
    "exp_id": "exp_id",
    "test": False,
    "debug": 0,
    "demo": "",
    "load_model": "",
    "resume": False,
    "data_path": [
        "/home/lab5017/centernet_env/CenterNet_att2/CenterNet2/data/gazebo/gazebo_train.json",
        "/home/lab5017/centernet_env/CenterNet_att2/CenterNet2/data/gazebo/gazebo_val.json"
    ],
    "dataset_type": "lol_watch_that",
    "working_dir": "/home/lab5017/det_env2/squeezedet_keras/expirements/interface_test_6_centernet/",
    "mean": [
        [
            [
                0.4078965485095978,
                0.4471930265426636,
                0.4702611565589905
            ]
        ]
    ],
    "std": [
        [
            [
                0.28863829374313354,
                0.27408164739608765,
                0.27809834480285645
            ]
        ]
    ],
    "gpus": [
        0
    ],
    "num_workers": 4,
    "not_cuda_benchmark": False,
    "seed": 317,
    "print_iter": 1,
    "hide_data_time": False,
    "save_all": False,
    "metric": "loss",
    "vis_thresh": 0.3,
    "debugger_theme": "white",
    "arch": "resdcn_18",
    "head_conv": 64,
    "down_ratio": 4,
    "lr": 0.0001,
    "lr_step": [
        90,
        120
    ],
    "num_epochs": 2,
    "batch_size": 8,
    "master_batch_size": 18,
    "num_iters": -1,
    "val_intervals": 1,
    "trainval": False,
    "flip_test": False,
    "test_scales": [
        1.0
    ],
    "nms": False,
    "K": 100,
    "not_prefetch_test": False,
    "fix_res": True,
    "keep_res": False,
    "not_rand_crop": False,
    "shift": 0.1,
    "scale": 0.4,
    "rotate": 0,
    "flip": 0.5,
    "no_color_aug": False,
    "aug_rot": 0,
    "aug_ddd": 0.5,
    "rect_mask": False,
    "kitti_split": "3dop",
    "mse_loss": False,
    "reg_loss": "l1",
    "hm_weight": 1,
    "off_weight": 1,
    "wh_weight": 0.1,
    "hp_weight": 1,
    "hm_hp_weight": 1,
    "dep_weight": 1,
    "dim_weight": 1,
    "rot_weight": 1,
    "peak_thresh": 0.2,
    "norm_wh": False,
    "dense_wh": False,
    "cat_spec_wh": False,
    "not_reg_offset": False,
    "agnostic_ex": False,
    "scores_thresh": 0.1,
    "center_thresh": 0.1,
    "aggr_weight": 0.0,
    "dense_hp": False,
    "not_hm_hp": False,
    "not_reg_hp_offset": False,
    "not_reg_bbox": False,
    "eval_oracle_hm": False,
    "eval_oracle_wh": False,
    "eval_oracle_offset": False,
    "eval_oracle_kps": False,
    "eval_oracle_hmhp": False,
    "eval_oracle_hp_offset": False,
    "eval_oracle_dep": False,
    "gpus_str": "0",
    "reg_offset": True,
    "reg_bbox": True,
    "hm_hp": True,
    "reg_hp_offset": True,
    "pad": 31,
    "num_stacks": 1,
    "chunk_sizes": [
        18
    ],
    "root_dir": "/home/lab5017/centernet_env/CenterNet_att2/CenterNet2/src/lib/../..",
    "data_dir": "/home/lab5017/centernet_env/CenterNet_att2/CenterNet2/src/lib/../../data",
    "exp_dir": "/home/lab5017/centernet_env/CenterNet_att2/CenterNet2/src/lib/../../exp/ctdet",
    "save_dir": "/home/lab5017/det_env2/squeezedet_keras/expirements/interface_test_6_centernet/",
    "debug_dir": "/home/lab5017/det_env2/squeezedet_keras/expirements/interface_test_6_centernet/debug",
    "num_classes": 4,  # irrelevant here
    "output_h": 200,
    "output_w": 200,
    "output_res": 200,
    "heads": {
        "hm": 4,
        "wh": 2,
        "reg": 2
    }}

    base_params.update(additional_params)
    print(base_params)
    with open(config_path, "w") as f:
        json.dump(base_params, f, sort_keys=False, indent=4)



if __name__ == "__main__":
    create_config("/home/lab5017/det_env2/squeezedet_keras/expirements/CN_kitti/CenterNet_kitti_2.json")
