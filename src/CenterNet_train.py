from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import src._init_paths

import os

import torch
import torch.utils.data
from opts import opts
from models.model import create_model, load_model, save_model
from models.data_parallel import DataParallel
from logger import Logger
from datasets.dataset_factory import get_dataset
from trains.train_factory import train_factory
from utils_cn.create_config import load_config
from src.test_and_visualize import test_and_visualize
import json
from src.lib.models.networks.lossnet import LossNet
from torch.utils.data.sampler import SubsetRandomSampler
from src.al_statistics import get_al_statistics
from matplotlib import pyplot as plt
import numpy as np
import random

# for reproducible results
def set_random_seeds(random_seed=317):
    torch.manual_seed(random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    np.random.seed(random_seed)
    random.seed(random_seed)


def main(opt):
  torch.manual_seed(opt.seed)
  torch.backends.cudnn.benchmark = not opt.not_cuda_benchmark and not opt.test
  Dataset = get_dataset(opt.dataset, opt.task)
  opt = opts().update_dataset_info_and_set_heads(opt, Dataset)
  print(opt)

  logger = Logger(opt)

  os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
  opt.device = torch.device('cuda' if opt.gpus[0] >= 0 else 'cpu')

  print('Creating model...')
  model = create_model(opt.arch, opt.heads, opt.head_conv)
  optimizer = torch.optim.Adam(model.parameters(), opt.lr)
  start_epoch = 0
  if opt.load_model != '':
    model, optimizer, start_epoch = load_model(
      model, opt.load_model, optimizer, opt.resume, opt.lr, opt.lr_step)

  Trainer = train_factory[opt.task]
  trainer = Trainer(opt, model, optimizer)
  trainer.set_device(opt.gpus, opt.chunk_sizes, opt.device)

  print('Setting up data...')
  val_loader = torch.utils.data.DataLoader(
      Dataset(opt, 'val'),
      batch_size=1,  # lol only batch size 1 for validation, errors otherwise
      shuffle=False,
      num_workers=1,
      pin_memory=True
  )

  if opt.test:
    _, preds = trainer.val(0, val_loader)
    val_loader.dataset.run_eval(preds, opt.save_dir)
    return

  train_loader = torch.utils.data.DataLoader(
      Dataset(opt, 'train'),
      batch_size=opt.batch_size,
      shuffle=True,
      num_workers=opt.num_workers,
      pin_memory=True,
      drop_last=True
  )

  print('Starting training...')
  best = 1e10
  loss_names = ["loss", "hm_loss", "wh_loss", "off_loss"]
  logger.write("epoch,loss,hm_loss,wh_loss,off_loss,val_loss,val_hm_loss,val_wh_loss,val_off_loss")
  logger.write('\n')
  for epoch in range(start_epoch + 1, opt.num_epochs + 1):
    print("epoch", epoch)
    mark = epoch if opt.save_all else 'last'
    log_dict_train, _ = trainer.train(epoch, train_loader)
    logger.write('{},'.format(epoch))
    for k, v in log_dict_train.items():
      logger.scalar_summary('train_{}'.format(k), v, epoch)
      logger.write('{:8f},'.format(v))
    if opt.val_intervals > 0 and epoch % opt.val_intervals == 0:

      with torch.no_grad():
        log_dict_val, preds = trainer.val(epoch, val_loader)
      tmp_str_list = []
      for k, v in log_dict_val.items():
        logger.scalar_summary('val_{}'.format(k), v, epoch)
        tmp_str_list.append('{:8f}'.format(v))
        # logger.write('{:8f}'.format(v))
      logger.write(",".join(tmp_str_list))
      if log_dict_val[opt.metric] < best:
        best = log_dict_val[opt.metric]
        if not opt.save_all:
            save_model(os.path.join(opt.save_dir, 'checkpoints/CenterNet_{}_best.pth'.format(opt.dataset)),
                   epoch, model)
      if  epoch % opt.saving_interval == 0:
        current_name = 'CenterNet_{}_{:03d}-{:.2f}'.format(opt.dataset, epoch, log_dict_val["loss"])
        save_model(os.path.join(opt.save_dir, 'checkpoints/CenterNet_{}_{:03d}-{:.2f}.pth'
                                .format(opt.dataset, epoch, log_dict_val["loss"])),
                 epoch, model, optimizer)
        logger.write_config(log_dict_val["loss"], current_name, opt)
    else:
        pass
      # save_model(os.path.join(opt.save_dir, 'checkpoints/CenterNet_last.pth'),
      #            epoch, model, optimizer)
    logger.write('\n')
    if epoch in opt.lr_step:
      # save_model(os.path.join(opt.save_dir, 'checkpoints/CenterNet_{}.pth'.format(epoch)),
      #            epoch, model, optimizer)
      lr = opt.lr * (0.1 ** (opt.lr_step.index(epoch) + 1))
      print('Drop LR to', lr)
      for param_group in optimizer.param_groups:
          param_group['lr'] = lr
  logger.close()


def main_AL(opt):
    set_random_seeds(opt.seed)
    torch.backends.cudnn.benchmark = not opt.not_cuda_benchmark and not opt.test
    Dataset = get_dataset(opt.dataset, opt.task)
    opt = opts().update_dataset_info_and_set_heads(opt, Dataset)
    print(opt)

    logger = Logger(opt)

    al_dir = os.path.join(opt.save_dir, 'al_progress/')
    if not os.path.exists(al_dir):
        os.mkdir(al_dir)
    for i in range(opt.num_cycles):
        if not os.path.exists(os.path.join(al_dir, "cycle_{}".format(i+1))):
            os.mkdir(os.path.join(al_dir, "cycle_{}".format(i+1)))

    os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
    opt.device = torch.device('cuda' if opt.gpus[0] >= 0 else 'cpu')

    print('Creating model...')
    model = create_model(opt.arch, opt.heads, opt.head_conv)
    optimizer = torch.optim.Adam(model.parameters(), opt.lr)

    model_loss_module = LossNet().to(opt.device)
    optimizer_module = torch.optim.Adam(model_loss_module.parameters(), opt.lr)

    start_epoch = 0
    if opt.load_model != '':
        model, optimizer, start_epoch = load_model(
            model, opt.load_model, optimizer, opt.resume, opt.lr, opt.lr_step)

    Trainer = train_factory[opt.task]
    trainer = Trainer(opt, model, optimizer=optimizer, model_module=model_loss_module,
                      optimizer_module=optimizer_module)
    trainer.set_device(opt.gpus, opt.chunk_sizes, opt.device)
    print('Setting up data...')


    train_dataset = Dataset(opt, 'train')
    val_dataset = Dataset(opt, 'val')
    labeled_indices = set(range(opt.cycle_images))  # init with first set of images
    unlabeled_indices = set(range(opt.cycle_images, len(train_dataset)))   # the residual is unlabeled
    al_stat_result = {"num_images": [], "map": []}  # dictionary for nam_samples/mAP statistics

    train_loader = torch.utils.data.DataLoader(
        Dataset(opt, 'train'),
        batch_size=opt.batch_size,
        sampler=SubsetRandomSampler(list(labeled_indices)),
        num_workers=opt.num_workers,
        pin_memory=True,
        drop_last=True
    )

    val_loader = torch.utils.data.DataLoader(
        val_dataset,
        batch_size=1,  # lol only batch size 1 for validation, errors otherwise
        shuffle=False,
        num_workers=1,
        pin_memory=True
    )

    if opt.test:
        _, preds = trainer.val(0, val_loader)
        val_loader.dataset.run_eval(preds, opt.save_dir)
        return


    print('Starting training...')
    best = 1e10
    loss_names = ["loss", "hm_loss", "wh_loss", "off_loss"]
    logger.write("epoch,loss,hm_loss,wh_loss,off_loss,val_loss,val_hm_loss,val_wh_loss,val_off_loss")
    logger.write('\n')
    epoch = 1
    for cycle in range(1, opt.num_cycles+1):
        for cycle_epoch in range(opt.cycle_epochs):
            print("epoch", epoch)
            mark = epoch if opt.save_all else 'last'
            log_dict_train, _ = trainer.train(epoch, train_loader)
            logger.write('{},'.format(epoch))

            for k, v in log_dict_train.items():
                logger.scalar_summary('train_{}'.format(k), v, epoch)
                logger.write('{:8f},'.format(v))
            if opt.val_intervals > 0 and epoch % opt.val_intervals == 0:

                with torch.no_grad():
                    log_dict_val, preds = trainer.val(epoch, val_loader)
                    print("PREDS", preds)
                    print(len(preds))
                tmp_str_list = []
                for k, v in log_dict_val.items():
                    logger.scalar_summary('val_{}'.format(k), v, epoch)
                    tmp_str_list.append('{:8f}'.format(v))
                    # logger.write('{:8f}'.format(v))
                logger.write(",".join(tmp_str_list))
                if log_dict_val[opt.metric] < best:
                    best = log_dict_val[opt.metric]
                    if not opt.save_all:
                        save_model(os.path.join(opt.save_dir, 'checkpoints/CenterNet_{}_best.pth'.format(opt.dataset)),
                                   epoch, model)
                        save_model(os.path.join(opt.save_dir, 'checkpoints/LossModule_{}_best.pth'.format(opt.dataset)),
                                   epoch, model_loss_module)
                if epoch % opt.saving_interval == 0:
                    current_name = 'CenterNet_{}_{:03d}-{:.2f}'.format(opt.dataset, epoch, log_dict_val["loss"])
                    save_model(os.path.join(opt.save_dir, 'checkpoints/CenterNet_{}_{:03d}-{:.2f}.pth'
                                            .format(opt.dataset, epoch, log_dict_val["loss"])),
                               epoch, model, optimizer)
                    # save_model(os.path.join(opt.save_dir, 'checkpoints/LossModule_{}_{:03d}-{:.2f}.pth'  #  breaks some logic later
                    #                         .format(opt.dataset, epoch, log_dict_val["loss"])),
                    #            epoch, model_loss_module, optimizer)
                    logger.write_config(log_dict_val["loss"], current_name, opt)
            else:
                pass
            if opt.cycle_epochs > 0 and epoch % opt.cycle_epochs == 0 and opt.random_sampling:
                if opt.active_learning:
                    raise ValueError("active_learning and random sampling are mutually exclusive")
                random_indices = list(unlabeled_indices)[0:opt.cycle_images]  # it's already quite random

                map_05 = get_al_statistics(vars(opt), model, data_path=opt.data_path,
                                           dataset_type=opt.dataset_type,
                                           working_dir=os.path.join(al_dir,"cycle_{}".format(cycle)))
                al_stat_result["num_images"].append(len(labeled_indices))
                al_stat_result["map"].append(map_05)

                print("map_05", map_05)
                logger.write("\nN_images: {}, mAP IoU 0.5: {}".format(len(labeled_indices), map_05))

                labeled_indices.update(set(random_indices))
                unlabeled_indices = unlabeled_indices - set(random_indices)

                train_loader = torch.utils.data.DataLoader(
                    Dataset(opt, 'train'),
                    batch_size=opt.batch_size,
                    sampler=SubsetRandomSampler(list(labeled_indices)),
                    num_workers=opt.num_workers,
                    pin_memory=True,
                    drop_last=True)

            if opt.cycle_epochs > 0 and epoch % opt.cycle_epochs == 0 and opt.active_learning:
                if opt.random_sampling:
                    raise ValueError("active_learning and random sampling are mutually exclusive")
                al_loader = torch.utils.data.DataLoader(
                    train_dataset,
                    batch_size=opt.batch_size,
                    sampler=SubsetRandomSampler(list(unlabeled_indices)),
                    num_workers=opt.num_workers,
                    pin_memory=True,
                    drop_last=True
                )
                log_dict_val, al_cycle_results = trainer.al_cycle(epoch, al_loader)
                if opt.active_learning_reverse:
                    al_cycle_results.sort(key=lambda res: res[0], reverse=True)
                else:
                    al_cycle_results.sort(key=lambda res: res[0])
                most_profitable = [res[2] for res in al_cycle_results[:10]]  # getting image paths
                least_profitable = [res[2] for res in al_cycle_results[-10:]]
                profitable_indices = [res[1] for res in al_cycle_results[0:opt.cycle_images]]

                map_05 = get_al_statistics(vars(opt), model, data_path=opt.data_path,
                                           dataset_type=opt.dataset_type,
                                           working_dir=os.path.join(al_dir,"cycle_{}".format(cycle)),
                                           most_prof=most_profitable, least_prof=least_profitable)
                al_stat_result["num_images"].append(len(labeled_indices))
                al_stat_result["map"].append(map_05)

                print("map_05", map_05)
                logger.write("\nN_images: {}, mAP IoU 0.5: {}".format(len(labeled_indices), map_05))

                labeled_indices.update(set(profitable_indices))
                unlabeled_indices = unlabeled_indices - set(profitable_indices)

                train_loader = torch.utils.data.DataLoader(
                    Dataset(opt, 'train'),
                    batch_size=opt.batch_size,
                    sampler=SubsetRandomSampler(list(labeled_indices)),
                    num_workers=opt.num_workers,
                    pin_memory=True,
                    drop_last=True)

            logger.write('\n')
            if epoch in opt.lr_step:
                lr = opt.lr * (0.1 ** (opt.lr_step.index(epoch) + 1))
                print('Drop LR to', lr)
                for param_group in optimizer.param_groups:
                    param_group['lr'] = lr
            epoch = epoch + 1
    logger.close()
    with open(os.path.join(al_dir, "al_progress.json"), "w") as write_file:
        json.dump(al_stat_result, write_file, sort_keys=False, indent=4)
    plt.plot(al_stat_result["num_images"], al_stat_result["map"], label='Active learning')
    plt.xlabel("Number of images")
    plt.ylabel("mAP IoU 0.5")
    plt.legend()
    plt.savefig(os.path.join(al_dir, "al_progress.png"))

def CenterNet_train(config):
      """
      Args:
          config: path to config with parameters for neural networks
      Returns: nope

      """
      cfg = load_config(config)
      print(cfg)

      opt = opts().parse(dict_args=cfg)
      main_AL(opt)

      checkpoint_dir = os.path.join(opt.save_dir, 'checkpoints/')
      weights_list = [x for x in os.listdir(checkpoint_dir) if x[x.rfind(".") + 1:] == "pth"]
      save_best_only = False
      if weights_list:
          if save_best_only:
              if 'CenterNet_{}_best.pth'.format(opt.dataset) in weights_list:
                  best_weights = os.path.join(checkpoint_dir, 'CenterNet_{}_best.pth'.format(opt.dataset))
              else:
                  print("no weights found to test!")
                  best_weights = None
          else:
              best_weights = weights_list[0]
              for weights in weights_list:
                  try:
                      current_loss = float(best_weights[best_weights.rfind("-", 0, len(best_weights) - 3) + 1:
                                                        best_weights.rfind("-", 0, len(best_weights) - 3) + 5])
                  except ValueError:
                      current_loss = 0
                  try:
                      new_loss = float(weights[weights.rfind("-", 0, len(weights) - 3) + 1:
                                               weights.rfind("-", 0, len(weights) - 3) + 5])
                  except ValueError:
                      new_loss = 0
                  if current_loss > new_loss:
                      best_weights = weights
      else:
          print("no weights found to test!")
          best_weights = None
      print("best_weights", best_weights)

      best_weights = os.path.join(opt.working_dir,  'checkpoints/{}'.format(best_weights))
      map_05 = test_and_visualize(config=vars(opt), weights=best_weights, data_path=opt.data_path,
                                  dataset_type=opt.dataset_type, working_dir=opt.working_dir, save_predicted=opt.save_predicted,
                                  visualize=opt.visualize, paint_gt=opt.paint_gt)
      print("map_05", map_05)
      # update configs with the value of map for iou = 0.5 for best weights
      best_config_name = os.path.join(checkpoint_dir, best_weights[:-4] + "_config.json")
      print("best_config_name", best_config_name)
      with open(best_config_name, "r") as jsonFile:
          best_config = json.load(jsonFile)

      best_config["test_map"] = map_05

      with open(best_config_name, "w") as write_file:
          json.dump(best_config, write_file, sort_keys=False, indent=4)


if __name__ == '__main__':

    pass
