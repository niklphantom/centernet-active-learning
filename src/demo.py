from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths

import os
import cv2

from opts import opts
from detectors.detector_factory import detector_factory
from utils.create_config import load_config
import json
import numpy as np

image_ext = ['jpg', 'jpeg', 'png', 'webp']
video_ext = ['mp4', 'mov', 'avi', 'mkv']
time_stats = ['tot', 'load', 'pre', 'net', 'dec', 'post', 'merge']

def demo(opt):
  os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
  opt.debug = max(opt.debug, 1)
  Detector = detector_factory[opt.task]
  detector = Detector(opt)

  save_dict = vars(opt).copy()
  save_dict.pop("device")
  for key, val, in save_dict.items():
      if type(val) is np.ndarray:
          save_dict[key] = val.tolist()
  with open(os.path.join(opt.save_dir, "/home/lab5017/centernet_env/CenterNet_att2/CenterNet2/models/initial_config.json"), "w") as write_file:
    json.dump(save_dict, write_file, sort_keys=False, indent=4)


  if opt.demo == 'webcam' or \
    opt.demo[opt.demo.rfind('.') + 1:].lower() in video_ext:
    cam = cv2.VideoCapture(0 if opt.demo == 'webcam' else opt.demo)
    detector.pause = False
    while True:
        _, img = cam.read()
        cv2.imshow('input', img)
        ret = detector.run(img)
        time_str = ''
        for stat in time_stats:
          time_str = time_str + '{} {:.3f}s |'.format(stat, ret[stat])
        print(time_str)
        if cv2.waitKey(1) == 27:
            return  # esc to quit
  else:
    if os.path.isdir(opt.demo):
      image_names = []
      ls = os.listdir(opt.demo)
      for file_name in sorted(ls):
          ext = file_name[file_name.rfind('.') + 1:].lower()
          if ext in image_ext:
              image_names.append(os.path.join(opt.demo, file_name))
    else:
      image_names = [opt.demo]
    
    for (image_name) in image_names:
      ret = detector.run(image_name)
      time_str = ''
      for stat in time_stats:
        time_str = time_str + '{} {:.3f}s |'.format(stat, ret[stat])
      print(time_str)
      print(ret["results"])

if __name__ == '__main__':
  # dict_args = {"demo": "/home/lab5017/Datacastle/develop/gazebo_dataset_5/real/",
  #              "load_model": "/home/lab5017/det_env2/squeezedet_keras/expirements/interface_test_6_centernet/checkpoints/CenterNet_gazebo_010-1.04.pth"}
  #
  # config = "/home/lab5017/det_env2/squeezedet_keras/expirements/interface_test_6_centernet/checkpoints/CenterNet_gazebo_010-1.04_config.json"
  # cfg = load_config(config)
  # cfg.update(dict_args)
  # # opt = opts().init()
  # opt = opts().parse(dict_args=cfg)
  # print("opt_debug", opt.debug)
  # print("load_model", opt.load_model)
  opt = opts().init()
  demo(opt)
