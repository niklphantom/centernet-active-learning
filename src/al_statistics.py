from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# from utils.utils import save_anno_xml

import src._init_paths

import  numpy as np
import os
import cv2

from opts import opts
from detectors.detector_factory import detector_factory
from utils.data_pipeline import get_annotations_lists
from src.visualization import predict_and_visualize
from compute_metrics.plot_metrics import Stat, plot_graphics
from utils_cn.create_config import load_config
from datasets.dataset_factory import get_dataset
import shutil


image_ext = ['jpg', 'jpeg', 'png', 'webp']
video_ext = ['mp4', 'mov', 'avi', 'mkv']
time_stats = ['tot', 'load', 'pre', 'net', 'dec', 'post', 'merge']
color_list = [ (255,255,255), (255, 10,10), (0,0,255), (0,255,0), (13, 199, 255), (10, 230, 250), (5, 131, 250)]   # TODO make it able to have arbitrary number of colors




def get_al_statistics(config, model, data_path, dataset_type, working_dir, visualize=False, save_predicted=False,
                       split_ratio=[70, 10, 20], save_splits=True, paint_gt=False, most_prof=None, least_prof=None):
    """
    Allows to test trained model and visualise predictions on a test dataset
    Args:
        config: path to config with parameters for neural networks
        weights: weights of a trained model
        data_path: path to files characterizing dataset depending on "dataset_type" variable
        dataset_type: one of ["xml_main_dirs", "xml_txt_splits", "xml_txt_main_lists", "xml_test_dirs", "xml_txt_test_splits", "test_list"]
        "test_list" options is used if it is needed to pass test list returned from get_annotation_lists function
        working_dir: directory where plots and predictions will be saved
        visualize: if predictions on a test dataset should be visualized and saved as .jpg images in images/ subdirectory of working_dir
        save_predicted: if predictions on a test dataset should be saved inside working directory in pred/ folder
        in xml format
        split_ratio: split_ratio: split ratio in case provided data should be splitted on a train, val, test
        save_splits: if created splits should be represented as txt files and saved
        paint_gt: if ground truth should be painted on visualizations (white color)

    Returns: mAP for IoU = 0.5 (float)

    """
    #  debug = 1 is for using cv2.imshow for every picture
    dict_args = {
                 "debug": 0}
    if isinstance(config, str):   #  in case you want to pass it as a path to config, my dear
        cfg = load_config(config)
    else:
        cfg = config
    cfg.update(dict_args)
    opt = opts().parse(dict_args=cfg)

    os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
    Dataset = get_dataset(opt.dataset, opt.task)
    opt = opts().update_dataset_info_and_set_heads(opt, Dataset)
    # opt.debug = max(opt.debug, 1) # set debug manually here you dumbf&*#er
    Detector = detector_factory[opt.task]
    detector = Detector(opt, model=model)



    dataset_types = ["xml_main_dirs", "xml_txt_splits", "xml_txt_main_lists", "xml_test_dirs", "xml_txt_test_splits", "test_list"]
    assert dataset_type in dataset_types


    if dataset_type in ["xml_test_dirs", "xml_txt_test_splits"]:
        # test_list = get_annotations_lists(data_path=data_path, dataset_type=dataset_type, config=config, split_ratio=split_ratio, save_splits=save_splits)
        test_list = get_annotations_lists(data_path=data_path, dataset_type=dataset_type, config=config, save_dir=working_dir,
                                          split_ratio=split_ratio, save_splits=save_splits, rescale=True)
    elif dataset_type == "test_list":
        test_list = data_path
    else:
        print("Shuffling initial list again, result will be the same if random seed is the same")
        _, _, test_list = get_annotations_lists(data_path=data_path, dataset_type=dataset_type, config=config, save_dir=working_dir + "trick/",
                                                split_ratio=split_ratio, save_splits=save_splits, rescale=True)

    predictions_list = []

    BATCH_SIZE = 1
    _, last_batch_size = divmod(len(test_list), BATCH_SIZE)
    for i in range(len(test_list) // BATCH_SIZE):
        img_names_batch = []
        annotations_batch = []
        for j in range(BATCH_SIZE):
            img_name = test_list[i*BATCH_SIZE+j][0]
            img_names_batch.append(img_name)
            annotations_batch.append(test_list[i*BATCH_SIZE+j][1])


        # imgs, preds = predict_and_visualize(model, img_names_batch, cfg, y_true=annotations_batch, working_dir=working_dir,
        #                                        save_predicted=save_predicted, paint_gt=paint_gt)
        preds = predict_and_visualize(detector_or_weights=detector, config=cfg, img_names=img_names_batch, working_dir=working_dir,
                              y_true=annotations_batch, save_predicted=save_predicted, visualize=visualize, opt=opt, paint_gt=paint_gt)

        predictions_list.extend(preds)

    if last_batch_size:    # get predictions for images which were residuals to division
        img_names_batch = [x[0] for x in test_list[-last_batch_size:]]
        annotations_batch = [x[1] for x in test_list[-last_batch_size:]]
        # imgs, preds = predict_and_visualize(model, img_names_batch, cfg, y_true=annotations_batch, working_dir=working_dir,
        #                                        save_predicted=save_predicted, paint_gt=paint_gt, last_batch_size=last_batch_size)
        preds = predict_and_visualize(detector_or_weights=detector, config=cfg, img_names=img_names_batch, working_dir=working_dir,
                              y_true=annotations_batch, save_predicted=save_predicted, visualize=visualize, paint_gt=paint_gt)
        predictions_list.extend(preds)

    if most_prof:
        if not os.path.exists(os.path.join(working_dir, "most_prof/pred/")):
            os.makedirs(os.path.join(working_dir, "most_prof/pred/"))
        if not os.path.exists(os.path.join(working_dir, "most_prof/images/")):
            os.makedirs(os.path.join(working_dir, "most_prof/images/"))
        for path in most_prof:
            img_name = os.path.split(path)[1]
            shutil.copy(path, os.path.join(working_dir, "most_prof/images/", img_name))
            # to visualize preds
            _ = predict_and_visualize(detector_or_weights=detector, config=cfg, img_names=[path],
                                          working_dir=os.path.join(working_dir, "most_prof/pred/"),
                                          y_true=annotations_batch, save_predicted=save_predicted, visualize=True,
                                          paint_gt=paint_gt)

    if least_prof:
        if not os.path.exists(os.path.join(working_dir, "least_prof/pred/")):
            os.makedirs(os.path.join(working_dir, "least_prof/pred/"))
        if not os.path.exists(os.path.join(working_dir, "least_prof/images/")):
            os.makedirs(os.path.join(working_dir, "least_prof/images/"))
        for path in least_prof:
            img_name = os.path.split(path)[1]
            shutil.copy(path, os.path.join(working_dir, "least_prof/images/", img_name))
            # to visualize preds
            _ = predict_and_visualize(detector_or_weights=detector, config=cfg, img_names=[path],
                                          working_dir=os.path.join(working_dir, "least_prof/pred/"),
                                          y_true=annotations_batch, save_predicted=save_predicted, visualize=True,
                                          paint_gt=paint_gt)


    classes = cfg["classes"]
    print(classes)

    mAP_threshold = (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.75, 0.8)
    min_confidences = (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95)
    image_folder = working_dir
    map_05 = plot_graphics(test_list, predictions_list, classes, mAP_threshold, min_confidences, working_dir)

    return map_05

if __name__ == '__main__':
    pass