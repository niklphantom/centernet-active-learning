from src.CenterNet_train import CenterNet_train
import argparse


if __name__ == "__main__":
    parser= argparse.ArgumentParser()
    parser.add_argument("config")
    args = parser.parse_args()
    CenterNet_train(args.config)

