import xml.etree.cElementTree as ET

def parse_gt_xml(gt_file):
    annotations = []
    ext = gt_file[-3:]
    
    if ext == 'xml':
        tree = ET.ElementTree(file=gt_file)
        root = tree.getroot()
        
        bbox_transform_inv = lambda box: (box[0], box[1], box[2] - box[0], box[3] - box[1])
        
        for child_of_root in root.iterfind('object'):
            for item in child_of_root.iterfind('./name'):
                name = item.text
                cls = name.lower().strip()
                #cls = config.CLASS_TO_IDX[name.lower().strip()]
            for item in child_of_root.iterfind('./bndbox//'):
                if item.tag == 'xmin':
                    xmin = float(item.text)
                elif item.tag == 'ymin':
                    ymin = float(item.text)
                elif item.tag == 'xmax':
                    xmax = float(item.text)
                elif item.tag == 'ymax':
                    ymax = float(item.text)
            #check for valid bounding boxes
            assert xmin >= 0.0 and xmin <= xmax, \
                'Invalid bounding box x-coord xmin {} or xmax {} at {}' \
                    .format(xmin, xmax, gt_file)
            assert ymin >= 0.0 and ymin <= ymax, \
                'Invalid bounding box y-coord ymin {} or ymax {} at {}' \
                    .format(ymin, ymax, gt_file)
            x, y, w, h = bbox_transform_inv([xmin, ymin, xmax, ymax])
            annotations.append([x, y, w, h, cls])
            
    return annotations


if __name__ == "__main__":
    anns = parse_gt_xml('E:\\work\\dataset_from_gazebo_3\\train\\labels\\img_0000.xml')
    print(anns)