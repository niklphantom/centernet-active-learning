import os
import numpy as np
import shutil
import random
from xml.etree.ElementTree import Element, ElementTree, SubElement, parse

def getFormat(name):
    '''
    Берет на вход имя (str),
    возвращает подстроку-формат (str) и позицию с конца (отрицательное число)
    БЕЗ ТОЧКИ

    Примеры:
    getFormat("blablabla.jpg") -> 'jpg', -3
    getFormat("some_name.suffix.json") -> 'json', -4
    '''
    k = name.rfind('.')
    name = name[k + 1:]
    k = -(len(name))
    return name, k

def get_roi_image(image_w, image_h, number, target_w=608, target_h=608, horiz_offset=(-100,100)):
    '''
    Функция выбора ОИ (области интереса) на изображении.
    Принимает на вход размеры изображения, целевые размеры, и некоторые параметры модификации
    Возвращает флаг правильности исполнения и список рамок с ОИ

    image_w: int, ширина изображения
    image_h: int, высота изображения
    number: int, число ОИ, которые будем вырезать из изображения
    target_w: int, ширина ОИ
    target_h: int, высота ОИ
    horiz_offset: tuple(int<=0, int>=0), величина допустимого сдвига центра ОИ по горизонтали относительно центра
    изображения В ПРОЦЕНТАХ. Если (0, 0), то number=1 и будет выбрана одна ОИ по центру изображения.

    Пример:
    get_roi_image(image_w=1280, image_h=720, number=3, target_w=608, target_h=608, horiz_offset=(-100,100))
    Смещение +-100%. Это значит, что будет выбран отрезок, на котором могут размещаться центры ОИ так, что
    в его левом конце ОИ будет прилегать к левому краю изобр-я, а в правом конце - к правому. Этот отрезок делится
    на 3 (number) части или суботрезка. Теперь выбираем центр каждой из трех ОИ как рандомную точку с каждого из
    суботрезков. Зная центры ОИ и их размеры, получаем искомое.
    '''

    # Имеем опорные значения - точка центра ОИ и её возможные отклонения,
    # и размеры (ширина, высота) ОИ и их возможные отклонения

    # Координаты центра. Координата hc изменяться не будет
    xc = image_w // 2 + image_w % 2
    yc = image_h // 2 + image_h % 2

    if target_w == target_h:
        if image_h < image_w:
            max_h = image_h
            max_w = image_h
        else:
            max_h = image_w
            max_w = image_w
    else:
        print('Пока что можно обрезать только квадратиком, простите!')
        return False, None

    min_h = target_h
    min_w = target_w

    offset_l, offset_r = horiz_offset
    if offset_l == 0 and offset_r == 0:
        print('При таких параметрах сдвига можем сделать только один кадр!')
        number = 1
    assert (offset_l <= 0 or offset_r >= 0), 'Некорректные значения сдвига!'
    assert (offset_l < -100 or offset_r > 100), 'Каждое из значений horizontal_offset должно быть не больше 100 (%) по модулю!'

    # Находим значения сдвига в пикселях
    offset_l = xc + int(np.round(float(offset_l) / 100.0 * (image_w - max_w) / 2))
    offset_r = xc + int(np.round(float(offset_r) / 100.0 * (image_w - max_w) / 2))

    # Здесь уже рандомом выбираем конкретные ОИ в количестве number
    delta = (offset_r-offset_l)/number
    roi_cx = [random.randint(offset_l + int(delta*n), offset_l + int(delta*(n+1))) for n in range(number)]
    roi_cy = [yc    for n in range(number)]
    # Это изменится, если реализую zoom (нет)
    roi_w  = [max_w for n in range(number)]
    roi_h  = [max_h for n in range(number)]
    if not len(roi_cx)==len(roi_w)==len(roi_h)==len(roi_cy):
        print('Размеры и значения положения созданы для разного числа вырезок!')
        return False, None

    # Рассчитаем итоговые координаты ОИ
    boxes = []
    for cx, cy, w, h in zip(roi_cx, roi_cy, roi_w, roi_h):
        x1 = cx-(w//2)
        x2 = cx+(w//2)
        y1 = cy-(h//2)
        y2 = cy+(h//2)
        boxes.append((x1, y1, x2, y2))

    return True, boxes

def intersection(roi, obj):
    '''
    Степень пересечения ОИ с объектом.
    Принимает на вход списки из 4 значений [xmin, ymin, xmax, ymax]:

    roi: list(int, int, int, int)
    obj: list(int, int, int, int)

    Если не пересекаются, возвращает вещественный 0.
    Если пересекаются, возвращает отношение площади пересечения к площади объекта.
    То есть, если объект полностью попадает в ОИ, то результат будет 1.0, т.е. 100%.
    Если половина объекта оказалась в ОИ, то результат будет 0.5, т.е. 50%.
    '''

    roi_x1, roi_y1, roi_x2, roi_y2 = roi
    obj_x1, obj_y1, obj_x2, obj_y2 = obj
    w = min(roi_x2, obj_x2) - max(roi_x1, obj_x1)
    h = min(roi_y2, obj_y2) - max(roi_y1, obj_y1)
    res = 0.
    if not ((w < 0) or (h < 0)):
        res = w*h / float((obj_x2-obj_x1)*(obj_y2-obj_y1))
    return res

def getObjCoordsInROI(roi_box, obj_box, threshold=1.):
    '''
    Проверяем, входит ли объект в ОИ, и если входит, возвращаем новые координаты объекта.
    Если не входит, возвращаем None

    roi_box: list(xmin, ymin, xmax, ymax), все int - координаты ОИ
    obj_box: list(xmin, ymin, xmax, ymax), все int - координаты объекта
    threshold: от 0. до 1., float - минимальная часть площади объекта, которая должна находиться в ОИ, чтобы считать,
                                    что объект входит в ОИ.

    Возвращает list(xmin, ymin, xmax, ymax), все int, либо None
    '''

    # Проверим, сколько процентов объекта остается в ОИ
    part = intersection(roi=roi_box, obj=obj_box)
    if part < threshold:
        return None
    else:
        roi_x1, roi_y1, roi_x2, roi_y2 = roi_box
        obj_x1, obj_y1, obj_x2, obj_y2 = obj_box
        obj_x1 = max(obj_x1, roi_x1) - roi_x1
        obj_y1 = max(obj_y1, roi_y1) - roi_y1
        obj_x2 = min(obj_x2, roi_x2) - roi_x1
        obj_y2 = min(obj_y2, roi_y2) - roi_y1
        return (obj_x1, obj_y1, obj_x2, obj_y2)

def get_roi_anno(roi_box, obj_boxes, target_classes=(), ignored_classes=(), rename_as='', min_overlap=1):
    '''
    Функция, которая принимает на вход координаты одной ОИ, координаты объектов, и пересчитывает координаты
    объектов относительно ОИ. Объекты, которые в ОИ не входят, отбрасываются.

    roi_box: list(xmin, ymin, xmax, ymax), все int - координаты ОИ
    obj_box: list(tuple([confidence(float),] [objid(str),] classname(str), x1(int), y1(int), x2(int), y2(int))),
             то, что в квадратных скобках, опционально, т.е. может не присутствовать.
    target_classes: tuple(str), классы, которые берем, сохраняя их имя
    ignored_classes: tuple(str), объекты каких классов не будем учитывать
    rename_as: str, если задано, то все классы, не входящие в целевые и игнор, будут названы этим именем и будут взяты
    min_overlap: float, от 0 до 1, порог для функции getObjCoordsInROI(roi_box, obj_box, threshold=1.)

    Возвращаем
    new_obj_boxes: list(tuple([confidence(float),] [objid(str),] classname(str), x1(int), y1(int), x2(int), y2(int)))
    '''

    new_obj_boxes = []
    if len(target_classes)>0 and len(ignored_classes)>0:
        intersect = (set(target_classes) - set(ignored_classes)) + (set(ignored_classes) - set(target_classes))
        assert len(intersect)==0, 'target_classes и ignored_classes не должны пересекаться!'

    for obj_box in obj_boxes:
        # Скопируем бокс, чтобы не менять оригинальный
        new_obj_box = list(obj_box)
        # ФИЛЬТРАЦИЯ
            # если класс объекта в числе игнорируемых, отбросим
        if len(ignored_classes)>0 and     (new_obj_box[-5] in ignored_classes):
            continue
            # если этот класс не в числе игнорируемых или целевых, то переименуем его, если есть имя, иначе отбросим
        if len(target_classes)>0  and not (new_obj_box[-5] in target_classes):
            if rename_as:
                new_obj_box[-5] = rename_as
            else:
                continue
            # если целевые классы не заданы, но есть дефолтное имя, то переименуем и отправим в список объектов
        if len(target_classes)==0 and     rename_as:
            new_obj_box[-5] = rename_as
        # Проверяем, входит ли оъект в ОИ, и если да, какие у него будут относительные координаты
        res = getObjCoordsInROI(roi_box=roi_box, obj_box=new_obj_box[-4:], threshold=min_overlap)
        if res:
            # если входит, добавляем в иоговый список для сохранения в аннотациях
            new_obj_box[-4] = res[0]
            new_obj_box[-3] = res[1]
            new_obj_box[-2] = res[2]
            new_obj_box[-1] = res[3]
            new_obj_boxes.append(new_obj_box)

    return new_obj_boxes

def parse_annotation_xml(anno_path, force_add_conf=False):
    '''
    Парсит xml аннотацию в формате VOC PASCAL.

    Принимает на вход путь к xml файлу.

    Возвращает имя (только имя без директорий) изображения, соответствующего аннотации, ширину изображения, высоту
    изображения, список боксов:
    imgname, img_w, img_h, img_d, boxes
    Список боксов имеет формат (значения в квадратнвх скобках опциональны, т.е. могут не присутствовать):
    boxes: list(tuple([confidence(float),] [objid(str),] classname(str), x1, y1, x2, y2))
    '''
    # Вид каждого элемента в списке boxes:
    #
    img_w = 0
    img_h = 0
    imgname = ''
    boxes = []

    tree = parse(anno_path)

    for elem in tree.iter():
        if 'filename' in elem.tag:
            imgname = elem.text
        if 'width' in elem.tag:
            img_w = int(elem.text)
        if 'height' in elem.tag:
            img_h = int(elem.text)
        if 'depth' in elem.tag:
            img_d = int(elem.text)
        if 'object' in elem.tag or 'part' in elem.tag:
            box = ['', '', '', 0, 0, 0, 0]

            for attr in list(elem):
                if 'confidence' in attr.tag:
                    box[0] = float(attr.text)
                else:
                    if force_add_conf:
                        box[0] = 1.0
                if 'id' in attr.tag:
                    box[1] = int(round(float(attr.text)))
                if 'name' in attr.tag:
                    box[2] = attr.text

                if 'bndbox' in attr.tag:
                    for dim in list(attr):
                        if 'xmin' in dim.tag:
                            box[3] = int(round(float(dim.text)))
                        if 'ymin' in dim.tag:
                            box[4] = int(round(float(dim.text)))
                        if 'xmax' in dim.tag:
                            box[5] = int(round(float(dim.text)))
                        if 'ymax' in dim.tag:
                            box[6] = int(round(float(dim.text)))
            if box[2] and (box[5] > 0) and (box[6] > 0):
                # Если id и confidence так и не было заполнено, то удалим их
                if not box[1]:
                    del box[1]
                if not box[0]:
                    del box[0]
                boxes.append(box)

    return imgname, img_w, img_h, img_d, boxes

def save_anno_xml(dir,
                  img_name,
                  img_format,
                  img_w,
                  img_h,
                  img_d,
                  boxes,
                  minConf=-1.,
                  quiet=False):

    '''
    Функция, которая сохраняет аннотации в xml файле в формате VOC PASCAL

    dir: str, папка, куда сохраняются аннотации.
    img_name: str, имя аннотации БЕЗ ТОЧКИ И РАСШИРЕНИЯ.
    img_format: str, расширение изображения БЕЗ ТОЧКИ.
    img_w: int, ширина изображения.
    img_h: int, высота изображения.
    img_d: int, число каналов изображения, обычно 3.
    boxes:      list(tuple([confidence(float),] [objid(str),] classname(str), x1(int), y1(int), x2(int), y2(int)))
    minConf:    минимально допустимое значение уверенности, которое должно быть у бокса.
                если = -1., значит, у боксов вообще нет его.
    quiet: bool, сохранять ли "молча", т.е. не выводить строки типа "сохранена аннотация ..."

    Ничего не возвращает.
    '''

    # Сохраним новую аннотацию
    annotation = Element('annotation')
    filename = SubElement(annotation, "filename")
    filename.text = img_name + '.' + img_format
    size = SubElement(annotation, "size")
    width = SubElement(size, "width")
    height = SubElement(size, "height")
    depth = SubElement(size, "depth")
    width.text = str(img_w)
    height.text = str(img_h)
    depth.text = str(img_d)
    if len(boxes) > -1:
        for box in boxes:
            conf  = -1.
            objid = -1
            if minConf >= 0:
                if len(box) == 6:
                    conf, classname, x1, y1, x2, y2 = box
                elif len(box) == 7:
                    conf, objid, classname, x1, y1, x2, y2 = box
                else:
                    print('Параметр boxes передан в неправильном формате!')
                    return
                # Фильтр по уверенности
                if conf < minConf:
                    continue

            else:
                if len(box) == 5:
                    classname, x1, y1, x2, y2 = box
                elif len(box) == 6:
                    objid, classname, x1, y1, x2, y2 = box
                else:
                    print('Параметр boxes передан в неправильном формате!')
                    return

            object = SubElement(annotation, "object")
            name = SubElement(object, "name")
            name.text = classname
            if int(objid) >= 0:
                id = SubElement(object, "id")
                id.text = str(objid)
            if minConf >= 0.:
                confidence = SubElement(object, "confidence")
                confidence.text = str(conf)
            bndbox = SubElement(object, "bndbox")
            xmin = SubElement(bndbox, "xmin")
            ymin = SubElement(bndbox, "ymin")
            xmax = SubElement(bndbox, "xmax")
            ymax = SubElement(bndbox, "ymax")
            xmin.text = str(x1)
            ymin.text = str(y1)
            xmax.text = str(x2)
            ymax.text = str(y2)
    et = ElementTree(annotation)
    et.write(os.path.join(dir, img_name + '.xml'))
    if not quiet:
        print("File {} has been saved".format(img_name))

def nb_objects_by_classes(labels_dir, imgs_dir=None, classes=None):
    '''
    Считает, сколько объектов есть в базе по каждому классу, выводит результат на экран и возвращает полученный словарь.

    labels_dir: str, папка с аннотациями в формате xml VOC PASCAL.
    imgs_dir:   str, папка с изображениями. Если задана, то берутся только те аннотации, к которым найдены изображения.
    classes:    dict, словарь вида {class_name1: number_of_instances1, class_name2: number_of_instances2, ...}.

    Возвращает словарь классов
    classes: {class_name1: number_of_instances1, class_name2: number_of_instances2, ...}
    '''

    annlist = os.listdir(labels_dir)
    annlist.sort()
    L = len(annlist)

    if imgs_dir:
        imglist = [ann[:ann.rfind(".")]+'.jpg' for ann in annlist]

        ilist = set(os.listdir(imgs_dir))
        l = len(set(imglist).intersection(ilist))
        print("Найдено {} аннотаций, с ними совпадает {} изображений".format(L, l))
    else:
        print("Найдено {} аннотаций".format(L))

    if classes is None:
        classes = {}
    else:
        classes = dict.fromkeys(classes, 0)

    for i, ann_file in enumerate(annlist):
        imgname, img_w, img_h, img_d, boxes = parse_annotation_xml(labels_dir+ann_file)

        if boxes:
            for box in boxes:
                if box[0] in classes:
                    classes[box[0]] += 1
                else:
                    classes[box[0]] = 1

        print('{}/{}, number of objects {}'.format(i+1, L, len(boxes)))

    print('number of classes:\n', classes)
    return classes
