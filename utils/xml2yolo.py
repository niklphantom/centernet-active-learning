from xml_bbox import parse_gt_xml
import os


classes = ['box', 'chair', 'fire', 'trash']
dict_classes = dict(zip(classes, list(range(0, len(classes)))))

anns_path = '/home/mir/vladislav/datasets/gazebo_640x640/dataset_from_gazebo_5/test/labels'
imgs_path = '/home/mir/vladislav/datasets/gazebo_640x640/dataset_from_gazebo_5/test/images'
img_type = 'jpg'
save_file = '../model_data/gazebo_640x640/gazebo_test_data_5.txt'

imgs_with_anns = [(os.path.join(imgs_path, ann[:-3] + img_type), os.path.join(anns_path, ann)) for ann in os.listdir(anns_path)]

with open(save_file, 'w') as f:
    for (img_name, ann_name) in imgs_with_anns:
        f.write(img_name)
        anns = parse_gt_xml(ann_name)

        for ann in anns:
            x, y, w, h, cls = ann
            f.write(' '+str(int(x))+','+str(int(y))+','+str(int(x+w))+','+str(int(y+h))+','+str(dict_classes[cls]))
        f.write('\n')
