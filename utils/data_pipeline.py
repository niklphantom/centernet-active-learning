import threading
import cv2
import numpy as np
import random
import xml.etree.cElementTree as ET
import os
from easydict import EasyDict as edict
import json
from tqdm import tqdm



def load_dict(path):
    """Loads a dictionary from a given path name

    Arguments:
        path {[type]} -- string of path

    Returns:
        config as dictionary of parameters
    """

    with open(path, "r") as f:
        cfg = json.load(f)  ### this loads the array from .json format

    # changes lists back
    for key, val, in cfg.items():

        if type(val) is list:
            cfg[key] = np.array(val)

    # cast do easydict
    cfg = edict(cfg)

    # create full anchors from seed
    # cfg.ANCHOR_BOX, cfg.N_ANCHORS_HEIGHT, cfg.N_ANCHORS_WIDTH = set_anchors(cfg)
    # cfg.ANCHORS = len(cfg.ANCHOR_BOX)

    # if you added a class in the config manually, but were to lazy to update
    cfg.len_classes = len(cfg.classes)
    cfg.class_to_idx = dict(zip(cfg.classes, range(cfg.len_classes)))

    return cfg

def _bbox_to_coco_bbox(bbox):
  return [(bbox[0]), (bbox[1]),
          (bbox[2] - bbox[0]), (bbox[3] - bbox[1])]

def read_clib(calib_path):
  f = open(calib_path, 'r')
  for i, line in enumerate(f):
    if i == 2:
      calib = np.array(line[:-1].split(' ')[1:], dtype=np.float32)
      calib = calib.reshape(3, 4)
      return calib


def list_of_datalists_to_cocojson(data_lists="", save_dir="", cfg="", split_names=""):
    if split_names == "":
        split_names = ["train", "val", "test"]
    for data_list, split_name in zip(data_lists, split_names):
        datalist_to_cocojson(data_list=data_list, save_dir=save_dir, cfg=cfg, split_name=split_name)

def datalist_to_cocojson(data_list="", save_dir="", cfg="", split_name=""):
    output_json_dict = {
        "images": [],
        "type": "instances",
        "annotations": [],
        "categories": []
    }
    bnd_id = 1  # START_BOUNDING_BOX_ID
    print('Start converting !')
    for i in tqdm(range(len(data_list))):
        filename = data_list[i][0]
        img_id = os.path.splitext(os.path.split(filename)[1])[0]
        img_info = {
            'file_name': filename,
            'height': int(cfg["input_res"][1]),
            'width': int(cfg["input_res"][0]),
            'id': img_id
        }
        output_json_dict['images'].append(img_info)

        for bbox in data_list[i][1]:
            xmin, ymin, xmax, ymax = int(bbox[0]), int(bbox[1]), int(bbox[2]), int(bbox[3])
            category_id = int(bbox[4]) + 1  # since coco indexes start at 1
            o_width = xmax - xmin
            o_height = ymax - ymin
            ann = {
                'area': o_width * o_height,
                'iscrowd': 0,
                'bbox': [xmin, ymin, o_width, o_height],
                'category_id': category_id,
                'ignore': 0,
                'segmentation': []  # This script is not for segmentation
            }
            ann.update({'image_id': img_id, 'id': bnd_id})
            output_json_dict['annotations'].append(ann)
            bnd_id = bnd_id + 1

    print(cfg["class_to_idx"].keys())
    for label, label_id in cfg["class_to_idx"].items():
        category_info = {'supercategory': 'none', 'id': label_id + 1, 'name': label}
        output_json_dict['categories'].append(category_info)
    with open(os.path.join(save_dir, cfg["dataset"] + "_" + split_name + ".json"), 'w') as f:
        output_json = json.dumps(output_json_dict, indent=4)
        f.write(output_json)



def get_annotations_lists(data_path, dataset_type, config, split_ratio=[70, 10, 20], save_splits=True, save_dir="", rescale=False, random_seed=4):
    '''
    Allow to get annotation lists in the following format: list of lists [path_to_image, np.array], where np.array has
    shape (n,5) and its lines contain n annotations in the format x1, y1, x2, y2, cls_idx (data_type = float32) using
    different data sources
    :param data_path: path to files characterizing dataset depending on "dataset_type" variable
    :param dataset_type: one of ["xml_main_dirs", "xml_txt_splits", "xml_txt_main_lists", "xml_test_dirs", "xml_txt_test_splits", "test_list"]
    :param config: config path
    :param split_ratio: split ratio in case provided data should be splitted (list of three floats which sums to 1, e.g. [0.9,0.09,0.01])
    :param save_splits: if created splits should be represented as txt files and saved
    :param save_dir: save directory for save splits
    :param rescale: If True annotations are rescaled to the size of input image according to input_res config parameter
    :return: one list of lists [path_to_image, np.array], where np.array has
    shape (n,5) and its lines contain n annotations in the format x1, y1, x2, y2, cls_idx (data_type = float32) in case
    dataset types are "xml_test_dirs" or "xml_txt_test_splits", and returns three such lists corresponding to train, val, test splits
    in case dataset types are "xml_main_dirs", "xml_txt_splits", "xml_txt_main_lists"
    '''
    if isinstance(config, str):   #  in case you want to pass it as a path to config, my dear
        cfg = load_dict(config)
    else:
        cfg = config
        cfg = edict(cfg)

    # loads annotations from file
    def load_annotation(gt_file):
        target_width = cfg.input_res[0]
        target_height = cfg.input_res[1]

        annotations = []
        ext = gt_file[gt_file.rfind(".") + 1:]
        if ext == 'xml':
            tree = ET.ElementTree(file=gt_file)
            root = tree.getroot()
            for item in root.iterfind('./size//'):
                if item.tag == 'width':
                    gt_img_w = float(item.text)
                elif item.tag == 'height':
                    gt_img_h = float(item.text)
                elif item.tag == 'depth':
                    gt_img_d = float(item.text)
            # print(gt_img_h, gt_img_w)
            for child_of_root in root.iterfind('object'):
                for item in child_of_root.iterfind('./name'):
                    name = item.text
                    # cls = config.CLASS_TO_IDX[name.lower().strip()]
                    if name in cfg["classes"]:
                        cls_idx = cfg.class_to_idx[name]
                    else:
                        cls_idx = -1
                for item in child_of_root.iterfind('./bndbox//'):
                    if item.tag == 'xmin':
                        xmin = float(item.text)
                    elif item.tag == 'ymin':
                        ymin = float(item.text)
                    elif item.tag == 'xmax':
                        xmax = float(item.text)
                    elif item.tag == 'ymax':
                        ymax = float(item.text)
                # check for valid bounding boxes
                assert xmin >= 0.0 and xmin <= xmax, \
                    'Invalid bounding box x-coord xmin {} or xmax {} at {}' \
                        .format(xmin, xmax, gt_file)
                assert ymin >= 0.0 and ymin <= ymax, \
                    'Invalid bounding box y-coord ymin {} or ymax {} at {}' \
                        .format(ymin, ymax, gt_file)
                # x, y, w, h = bbox_transform_inv([xmin, ymin, xmax, ymax])
                if rescale:
                    if target_width != gt_img_w or target_height != gt_img_h:
                        x_scale = target_width / gt_img_w
                        y_scale = target_height / gt_img_h
                        xmin, ymin, xmax, ymax = xmin*x_scale, ymin*y_scale, xmax*x_scale, ymax*y_scale
                if name in cfg["classes"]:  # skip objects if they are not listed in classes
                    annotations.append([xmin, ymin, xmax, ymax, cls_idx])
            annotations = np.array(annotations)
            if len(annotations) == 0:
                print("length of annotation is zero")

        elif ext == 'txt':
            with open(gt_file, 'r') as f:
                lines = f.readlines()
            f.close()

            # each line is an annotation bounding box
            for line in lines:

                obj = line.strip().split(' ')

                # get class, if class is not in listed, skip it
                try:
                    # cls = config.CLASS_TO_IDX[obj[0].lower().strip()]
                    cls_idx = cfg.class_to_idx[obj[0].lower().strip()]
                    # get coordinates
                    xmin = float(obj[1])
                    ymin = float(obj[2])
                    xmax = float(obj[3])
                    ymax = float(obj[4])

                    # check for valid bounding boxes
                    assert xmin >= 0.0 and xmin <= xmax, \
                        'Invalid bounding box x-coord xmin {} or xmax {} at {}' \
                            .format(xmin, xmax, gt_file)
                    assert ymin >= 0.0 and ymin <= ymax, \
                        'Invalid bounding box y-coord ymin {} or ymax {} at {}' \
                            .format(ymin, ymax, gt_file)

                    # transform to  point + width and height representation
                    # x, y, w, h = bbox_transform_inv([xmin, ymin, xmax, ymax])

                    annotations.append([xmin, ymin, xmax, ymax, cls_idx])

                except Exception as e:
                    print(e)
                    print("wtf_bitch")
                    continue

        return annotations

    dataset_types = ["xml_main_dirs", "xml_txt_splits", "xml_txt_main_lists", "xml_test_dirs", "xml_txt_test_splits", "test_list"]
    assert dataset_type in dataset_types
    img_types = ["jpg", "jpeg", "png", "gif", "tiff", "bmp"]
    if dataset_type == "xml_main_dirs":
        img_files = [os.path.join(data_path[0], x) for x in sorted(os.listdir(data_path[0])) if x[x.rfind(".") + 1:] in img_types]
        print("found {} images".format(len(img_files)))
        gt_files = [os.path.join(data_path[1], x) for x in sorted(os.listdir(data_path[1])) if x[x.rfind(".") + 1:] == "xml"]
        print("found {} annotations".format(len(gt_files)))
        assert len(img_files) == len(gt_files)

        annotations_list =[]
        tmp = list(zip(img_files, gt_files))
        random.Random(random_seed).shuffle(tmp)  # allowing to use random seed for reproducible splits
        img_files, gt_files = zip(*tmp) # tuples from this point
        for img_name, gt_name in zip(img_files, gt_files):
            annotations = load_annotation(gt_name)
            annotations_list.append([img_name, annotations])

    if dataset_type == "xml_test_dirs":
        img_files = [os.path.join(data_path[0], x) for x in sorted(os.listdir(data_path[0])) if x[x.rfind(".") + 1:] in img_types]
        print("found {} images".format(len(img_files)))
        gt_files = [os.path.join(data_path[1], x) for x in sorted(os.listdir(data_path[1])) if x[x.rfind(".") + 1:] == "xml"]
        print("found {} annotations".format(len(img_files)))
        assert len(img_files) == len(gt_files)

        test_list = []
        tmp = list(zip(img_files, gt_files))
        random.Random(random_seed).shuffle(tmp)
        img_files, gt_files = zip(*tmp) # tuples from this point
        for img_name, gt_name in zip(img_files, gt_files):
            annotations = load_annotation(gt_name)
            test_list.append([img_name, annotations])
        return test_list

    if dataset_type == "xml_txt_splits":
        with open(data_path[0], 'r') as f:
            img_train = f.read().splitlines()
        with open(data_path[1], 'r') as f:
            gt_train = f.read().splitlines()
        with open(data_path[2], 'r') as f:
            img_val = f.read().splitlines()
        with open(data_path[3], 'r') as f:
            gt_val = f.read().splitlines()
        with open(data_path[4], 'r') as f:
            img_test = f.read().splitlines()
        with open(data_path[5], 'r') as f:
            gt_test = f.read().splitlines()

        # print("img_test", img_test)
        if len(data_path) == 7:  # allow usage of relative paths
            base_dir = data_path[6]
            print(base_dir)
        else:
            base_dir = ""
        train_list = []
        for img_name, gt_name in zip(img_train, gt_train):
            img_name = os.path.join(base_dir, img_name)
            gt_name = os.path.join(base_dir, gt_name)
            annotations = load_annotation(gt_name)
            train_list.append([img_name, annotations])
        val_list = []
        for img_name, gt_name in zip(img_val, gt_val):
            img_name = os.path.join(base_dir, img_name)
            gt_name = os.path.join(base_dir, gt_name)
            annotations = load_annotation(gt_name)
            val_list.append([img_name, annotations])
        test_list = []
        for img_name, gt_name in zip(img_test, gt_test):
            img_name = os.path.join(base_dir, img_name)
            gt_name = os.path.join(base_dir, gt_name)
            annotations = load_annotation(gt_name)
            test_list.append([img_name, annotations])
        return train_list, val_list, test_list

    if dataset_type == "xml_txt_test_splits":
        with open(data_path[0], 'r') as f:
            img_test = f.read().splitlines()
        with open(data_path[1], 'r') as f:
            gt_test = f.read().splitlines()
        # print("img_test", img_test)
        if len(data_path) == 3:  # allow usage of relative paths
            base_dir = data_path[2]
        else:
            base_dir = ""
        test_list = []
        for img_name, gt_name in zip(img_test, gt_test):
            img_name = os.path.join(base_dir, img_name)
            gt_name = os.path.join(base_dir, gt_name)
            annotations = load_annotation(gt_name)
            test_list.append([img_name, annotations])
        return test_list


    if dataset_type == "xml_txt_main_lists":
        with open(data_path[0], 'r') as f:
            img_files = f.read().splitlines()
        with open(data_path[1], 'r') as f:
            gt_files = f.read().splitlines()

        if len(data_path) == 3:  # allow usage of relative paths
            base_dir = data_path[2]
        else:
            base_dir = ""
        print("found {} annotations".format(len(img_files)))
        assert len(img_files) == len(gt_files)

        annotations_list = []
        tmp = list(zip(img_files, gt_files))
        random.Random(random_seed).shuffle(tmp)
        img_files, gt_files = zip(*tmp) # tuples from this point
        for img_name, gt_name in zip(img_files, gt_files):
            img_name = os.path.join(base_dir, img_name)
            gt_name = os.path.join(base_dir, gt_name)
            annotations = load_annotation(gt_name)
            annotations_list.append([img_name, annotations])
    # print(main_table)
    print(len(annotations_list))

    if sum(split_ratio) < 1 or sum(split_ratio) > 100:
        raise ValueError('Percentages of train val split should sum to 100')
    #split already shuffled list
    n_train = int(np.floor(len(annotations_list) * split_ratio[0]/100))
    n_val = int(np.floor(len(annotations_list) * (split_ratio[0] + split_ratio[1])/100))
    train_list = annotations_list[0:n_train]
    val_list = annotations_list[n_train:n_val]
    test_list = annotations_list[n_val:]

    if save_splits and save_dir and dataset_type in dataset_types:

        img_train = img_files[0:n_train]
        gt_train = gt_files[0:n_train]
        img_val = img_files[n_train:n_val]
        gt_val = gt_files[n_train:n_val]
        img_test = img_files[n_val:]
        gt_test = gt_files[n_val:]

        save_names = ["img_train.txt", "gt_train.txt", "img_val.txt", "gt_val.txt", "img_test.txt", "gt_test.txt"]
        for save_name, table in zip(save_names, [img_train, gt_train, img_val, gt_val, img_test, gt_test]):
            with open(os.path.join(save_dir, save_name), 'w') as write_table:
                lines = []
                # print(table)
                # for line in table:
                #     tmp = str(line[0]) + " " + str(line[1])
                #     lines.append(tmp)
                write_table.write("\n".join(table))

    return train_list, val_list, test_list


if __name__ == "__main__":

    CONFIG = "/home/lab5017/det_env2/squeezedet_keras/expirements/Interface_test_testing/hardhat_config.json"
    data_path = ["/home/lab5017/Datacastle/develop/gazebo_dataset_5/test/images/", "/home/lab5017/Datacastle/develop/gazebo_dataset_5/test/labels/"]


    save_dir = "/home/lab5017/det_env2/squeezedet_keras/expirements/"

    data_path = ["/home/lab5017/Datacastle/develop/test_datasets/detection_test/img_train_rel.txt",
                 "/home/lab5017/Datacastle/develop/test_datasets/detection_test/gt_train_rel.txt",
                 "/home/lab5017/Datacastle/develop/test_datasets/detection_test/img_val_rel.txt",
                 "/home/lab5017/Datacastle/develop/test_datasets/detection_test/gt_val_rel.txt",
                 "/home/lab5017/Datacastle/develop/test_datasets/detection_test/img_val_rel.txt",
                 "/home/lab5017/Datacastle/develop/test_datasets/detection_test/gt_val_rel.txt",
                 "/home/lab5017/Datacastle/develop/test_datasets/detection_test/"]
    # train_list, val_list,
    train_list, val_list, test_list = get_annotations_lists(data_path=data_path, dataset_type="xml_txt_splits", config=CONFIG, split_ratio=[0.9,0.09,0.01], save_dir = save_dir, save_splits=True)
    # print(wtf)
    print("train_list", train_list)

    print(test_list[-1][0])
    print(test_list[-1][1])
    print(test_list[-1][1][0][0])