from xml_bbox import parse_gt_xml
import os
import cv2


anns_path = '/home/mir/vladislav/datasets/gazebo/dataset_from_gazebo_5/test/labels'
imgs_path = '/home/mir/vladislav/datasets/gazebo/dataset_from_gazebo_5/test/images'
img_type = 'jpg'

imgs_with_anns = [(os.path.join(imgs_path, ann[:-3] + img_type), os.path.join(anns_path, ann)) for ann in os.listdir(anns_path)]
print(imgs_with_anns)
for (img_name, ann_name) in imgs_with_anns:
    img = cv2.imread(img_name)
    anns = parse_gt_xml(ann_name)

    for ann in anns:
        x, y, w, h, cls = ann
        print(x, y, w, h, cls)
        cv2.rectangle(img, (int(x), int(y)), (int(x) + int(w), int(y) + int(h)), (255, 0, 0), 4)

    cv2.imshow(f'Show annotation {img_name}', img)
    if cv2.waitKey(0)& 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break

    cv2.destroyAllWindows()
