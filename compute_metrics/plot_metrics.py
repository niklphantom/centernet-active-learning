import os
import numpy as np
import csv
import math
from compute_metrics.utils import parse_annotation_xml
from compute_metrics.compute_metrics import get_numbers, get_confMatrix
import matplotlib.pyplot as plt
import seaborn as sn
import  json


class Stat:

    def __init__(self, anno_gt, anno_pred, classes, mAP_thresh = (0.5,)):
        self.classes = classes
        self.anno_gt = anno_gt
        self.anno_pred = anno_pred

        assert (isinstance(mAP_thresh, tuple) or (isinstance(mAP_thresh, list))), \
            "parameter mAP_thresh in Stat() can be only a tuple or a list!"
        self.mAP_thresh = mAP_thresh
        self.AP_values = [[c, []] for c in classes] # уже не требуется инициализация
        self.mAP_values = {c:0 for c in classes}    # уже не требуется инициализация

        # print('Найдено {} совпадающих пар аннотаций'.format(self.l_match))
    def convert_boxes (self, annotation, prediction):
        """
        converts pair of annotations and predictions from np.array with lines x1, y1, x2, y2, cls_idx and x1, y1, x2, y2, cls_idx , cnf, to
        list(classname(str), x1, y1, x2, y2)) and  list(tuple([confidence(float),] classname(str), x1, y1, x2, y2))
        :param annotation:
        :param prediction:
        :return: list(classname(str), x1, y1, x2, y2)) and  list(tuple([confidence(float),] classname(str), x1, y1, x2, y2))
        """
        gt_boxes = []
        pred_boxes = []
        for ann_b in annotation[1]:
            # print(ann_b)
            ann_b_tmp = [1, 2, 3, 4, 5]
            ann_b_tmp[0], ann_b_tmp[1], ann_b_tmp[2], ann_b_tmp[3], ann_b_tmp[4] = \
                self.classes[int(ann_b[4])], int(ann_b[0]), int(ann_b[1]), int(ann_b[2]), int(ann_b[3])
            gt_boxes.append(ann_b_tmp)
        for pred_b in prediction[1]:
            pred_b_tmp = [1, 2, 3, 4, 5, 6]
            pred_b_tmp[0], pred_b_tmp[1], pred_b_tmp[2], pred_b_tmp[3], pred_b_tmp[4], pred_b_tmp[5] = \
                pred_b[5], self.classes[int(pred_b[4])], int(pred_b[0]), int(pred_b[1]), int(pred_b[2]), int(pred_b[3])
            pred_boxes.append(pred_b_tmp)
        # print(gt_boxes)
        # print(pred_boxes)
        return gt_boxes, pred_boxes

    def AP(self, min_overlap, min_conf, strict_true=True, raw_nums=False):
        # Здесь мы считаем среднюю точность для заданного набора minIoU, minConfidence по всем изображениям

            numbers = np.zeros(shape=(len(self.classes), 3), dtype=int)

            for annotation, prediction in zip(self.anno_gt, self.anno_pred):
                # print(annotation, prediction)
                gt_boxes, pred_boxes = self.convert_boxes(annotation, prediction)

                # *Отфильтруем предсказанные боксы по порогу
                # В список попадают лишь те элементы, для которых условие верно (ф-я выдает True)

                pred_boxes = list(filter(lambda x: not(x[0] < min_conf), pred_boxes))


                # Теперь посчитаем метрики
                img_numbers = get_numbers(gt_boxes    = gt_boxes,
                                          pred_boxes  = pred_boxes,
                                          classes     = self.classes,
                                          min_conf    = min_conf,
                                          min_IoU     = min_overlap,
                                          strict_true = strict_true)
                l_gt = len(gt_boxes)
                l_pred = len(pred_boxes)
                true = np.sum(img_numbers[:, 0])
                fneg = np.sum(img_numbers[:, 1])
                fpos = np.sum(img_numbers[:, 2])

                numbers = np.add(numbers, img_numbers)
            #print(numbers)
            precision = numbers[:, 0].astype(float) / (numbers[:, 0] + numbers[:, 2]).astype(float)
            recall    = numbers[:, 0].astype(float) / (numbers[:, 0] + numbers[:, 1]).astype(float)

            precision = np.nan_to_num(precision)
            recall = np.nan_to_num(recall)

            for p in precision:
                if math.isnan(p):
                    p = 0.0
            for r in recall:
                if math.isnan(r):
                    r = 0.0

            if min_overlap in self.mAP_thresh:
                for i in range(len(self.classes)):
                    self.AP_values[i][-1].append([precision[i], recall[i]])

            if raw_nums:
                return precision, recall, numbers[:, 0], numbers[:, 1], numbers[:, 2]
            else:
                return precision, recall

    def mAP(self, min_overlap):
        # lol, this function is not even in use
        for annotation, prediction in zip(self.anno_gt, self.anno_pred):
            gt_boxes, pred_boxes = self.convert_boxes(annotation, prediction)

    def PR_curve(self, min_overlap, min_confidences, strict_true=True, raw_nums=False):
        pr = []

        for min_conf in min_confidences:
            result = self.AP(min_overlap=min_overlap,
                             min_conf=min_conf,
                             strict_true=strict_true,
                             raw_nums=raw_nums)
            pr.append(result)  # presicion, recall

        return pr

    def AP_dir(self, min_overlap, min_confidences, strict_true = True, raw_nums = False):
        self.AP_values = [[c, []] for c in self.classes]
        self.mAP_values = {c: 0 for c in self.classes}


        for min_conf in min_confidences:
            result = self.AP(min_overlap=min_overlap,
                                       min_conf=min_conf,
                                       strict_true=strict_true,
                                       raw_nums=raw_nums)

        if (min_overlap in self.mAP_thresh):
            mean_mAP = 0.
            factual_len = 0
            for for_one_class in self.AP_values:
                d = {}
                for pr in for_one_class[1]:
                    p, r = pr
                    if r in d:
                        if p > d[r]:
                            d[r] = p
                    else:
                        d[r] = p

                min = 0.

                ks = sorted(d.keys())
                # print(ks, d[ks[0]])
                if ks[0] < 0:
                    continue

                if not (min in d):
                    d[min] = d[ks[0]]

                ks = sorted(d.keys())
                
                if d[min] == 0. and len(ks) > 1:
                    d[min] = d[ks[1]]

                factual_len += 1
                for r in ks:
                    if float(r) < 0:
                        self.mAP_values[for_one_class[0]] = -1
                    else:
                        a = d[min]*(r-min)
                        min = r
                        self.mAP_values[for_one_class[0]] += a
                a = d[min] * (1 - min)
                self.mAP_values[for_one_class[0]] += a
        lines = []
        for c in self.mAP_values.keys():
            mean_mAP += self.mAP_values[c]
            line = [c, self.mAP_values[c]]
            lines.append(line)

        mean_mAP /= factual_len
        lines.append(['all', mean_mAP])

        return lines

    def confusionMatrix(self, min_overlap, min_conf, strict_true = False):
        matclasses = list(self.classes)
        matclasses.append('none')
        matrix = np.zeros(shape=(len(matclasses), len(matclasses)), dtype=int)

        for annotation, prediction in zip(self.anno_gt, self.anno_pred):
            gt_boxes, pred_boxes = self.convert_boxes(annotation, prediction)

            # *Отфильтруем предсказанные боксы со слишком низкой достоверностью
            # В список попадают лишь те элементы, для которых условие верно (ф-я выдает True)
            pred_boxes_filtered = list(filter(lambda x: not(x[0] < min_conf), pred_boxes))

            # Теперь посчитаем матрицу
            img_matrix = get_confMatrix(gt_boxes   = gt_boxes,
                                        pred_boxes = pred_boxes_filtered,
                                        classes    = matclasses,
                                        min_conf   = min_conf,
                                        min_IoU    = min_overlap)

            matrix += img_matrix
        return matrix

def plot_graphics(anno_gt, anno_pred, classes, mAP_threshold, min_confidences, image_folder):
    if not os.path.exists(image_folder):
        os.mkdir(image_folder)

    large = 22;
    med = 16;
    small = 12
    params = {'axes.titlesize': large,
              'legend.fontsize': med,
              'axes.labelsize': med,
              'axes.titlesize': med,
              'xtick.labelsize': med,
              'ytick.labelsize': med,
              'figure.titlesize': large}
    plt.rcParams.update(params)

    statistics = Stat(anno_gt=anno_gt, anno_pred=anno_pred, classes=classes, mAP_thresh=mAP_threshold)
    mAP_iou05 = 0
    mAPs = []
    stats = []
    map_dict = {}
    for x in mAP_threshold:
        stat = statistics.AP_dir(x, min_confidences)
        map_dict["mAPs IoU {}:".format(x)] = {y[0]: y[1] for y in stat}
        if x == 0.5:
            mAP_iou05 = statistics.AP_dir(x, min_confidences)[-1][1]
            print("mAP_iou05", mAP_iou05)
        print("IoU thres: {}, maps:\n".format(x),stat)
        mAPs.append(stat[-1][1])
        stats.append(stat)

    with open(os.path.join(image_folder, "mAPs.json"), "w") as write_file:
        json.dump(map_dict, write_file, sort_keys=False, indent=4)

    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    ax.plot(mAP_threshold, mAPs)
    ax.set_title('mAP for all classes')
    ax.set_xlabel('IoU')
    ax.set_ylabel('mAP')
    ax.set_ylim([0, 1.05])
    ax.grid()
    fig.savefig(os.path.join(image_folder, 'mAP and IoU.png'))

    fig, ax = plt.subplots(1, 2)

    mAPs = statistics.AP_dir(0.5, min_confidences)
    y_pos = np.arange(len(mAPs))
    mAPs_val = [x[1] for x in mAPs]
    print("maps iou 0.5\n", mAPs_val)

    ax[0].grid(zorder=0)
    ax[0].barh(y_pos, mAPs_val, color='#96c1ff', align='center', height=0.5, zorder=3)
    ax[0].set_yticks(y_pos)
    ax[0].set_yticklabels([x[0] for x in mAPs])
    ax[0].invert_yaxis()  # labels read top-to-bottom
    ax[0].set_xlim([0, 1])
    ax[0].set_xlabel('AP')
    ax[0].set_title('Class AP (IoU = 0.5)')

    mAPs = statistics.AP_dir(0.75, min_confidences)
    y_pos = np.arange(len(mAPs))
    mAPs_val = [x[1] for x in mAPs]
    print("maps iou 0.7\n", mAPs_val)

    ax[1].grid(zorder=0)
    ax[1].barh(y_pos, mAPs_val, color='#96c1ff', align='center', height=0.5, zorder=3)
    ax[1].set_yticks(y_pos)
    ax[1].set_yticklabels(['' for x in mAPs])
    ax[1].invert_yaxis()  # labels read top-to-bottom
    ax[1].set_xlim([0, 1])
    ax[1].set_xlabel('AP')
    ax[1].set_title('Class AP (IoU = 0.75)')

    fig.savefig(os.path.join(image_folder, 'mAP bar plot.png'))

    labels = list(classes) + ['None']

    fig, axs = plt.subplots(ncols=2, nrows=2, figsize=(16, 18))

    list_IoU_thresh = [(0.5, 0.5), (0.5, 0.7), (0.7, 0.5), (0.7, 0.7)]

    for IoU_tresh, (i, j) in zip(list_IoU_thresh, zip([0, 0, 1, 1], [0, 1, 0, 1])):
        conf_mat = statistics.confusionMatrix(IoU_tresh[0], IoU_tresh[1])
        norm_conf_mat = conf_mat / np.asarray([x if x != 0 else 1 for x in conf_mat.astype(np.float).sum(axis=0)])

        heat_map = sn.heatmap(norm_conf_mat, xticklabels=labels, yticklabels=labels, annot=True, ax=axs[i][j],
                              cmap='Blues')
        heat_map.set_yticklabels(heat_map.get_yticklabels(), rotation=0)
        heat_map.set_xticklabels(heat_map.get_xticklabels(), rotation=45)
        heat_map.set_title("Confusion Matrix IoU = " + str(IoU_tresh[0]) + ", thres = " + str(IoU_tresh[1]))
        heat_map.set(xlabel='Fact label', ylabel='Predicted ylabel')

    fig.savefig(os.path.join(image_folder, 'cor_matrix_norm.png'))

    fig, axs = plt.subplots(ncols=2, nrows=2, figsize=(16, 18))
    # axs.ticklabel_format(useOffset=False, style='plain')
    print(conf_mat)
    for IoU_tresh, (i, j) in zip(list_IoU_thresh, zip([0, 0, 1, 1], [0, 1, 0, 1])):
        conf_mat = statistics.confusionMatrix(IoU_tresh[0], IoU_tresh[1])

        heat_map = sn.heatmap(conf_mat, xticklabels=labels, yticklabels=labels, annot=True, ax=axs[i][j], cmap='Blues', fmt="d")
        heat_map.set_yticklabels(heat_map.get_yticklabels(), rotation=0)
        heat_map.set_xticklabels(heat_map.get_xticklabels(), rotation=45)
        heat_map.set_title("Confusion Matrix IoU = " + str(IoU_tresh[0]) + ", thres = " + str(IoU_tresh[1]))
        heat_map.set(xlabel='Fact label', ylabel='Predicted ylabel')

    fig.savefig(os.path.join(image_folder, 'cor_matrix.png'))

    pr = statistics.PR_curve(0.5, min_confidences)
    print(pr)

    for j in range(math.ceil(len(classes) / 4)):
        fig, axs = plt.subplots(ncols=2, nrows=2, figsize=(16, 16))
        
        num = 4
        if len(classes) < (j+1) * 4:
            num = len(classes) % 4
        
        for i in range(num):
            prec = np.asarray([x[0][4 * j + i] for x in pr])
            rec = np.asarray([x[1][4 * j + i] for x in pr])

            prec = (rec == 0) * 1 + (rec != 0) * prec

            with open('pr.txt', 'w') as f:
                for (p, r), t in zip(zip(prec, rec), min_confidences):
                    f.write(str((p, r, t)) + '\n')
            axs[i // 2][i % 2].plot(rec, prec, label='pr-curve')
            #axs[i // 2][i % 2].plot(min_confidences, rec, label='recall')
            axs[i // 2][i % 2].grid()
            axs[i // 2][i % 2].set_title(classes[i])
            axs[i // 2][i % 2].legend()
            axs[i // 2][i % 2].set_xlabel('recall')
            axs[i // 2][i % 2].set_ylabel('precision')
            axs[i // 2][i % 2].set_ylim([0, 1.05])
        fig.suptitle('PR-curve', fontsize=22)
        fig.savefig(os.path.join(image_folder, 'PR_curve_' + str(j) + '.png'))

    return mAP_iou05

def convert_dir_to_list(xml_dir, classes_dict, is_gt=True):
    xml_list = set(os.listdir(xml_dir))
    annotation_list = []

    for xml_file in xml_list:
        imgname, img_w, img_h, img_d, boxes = parse_annotation_xml(os.path.join(xml_dir, xml_file))
        new_boxes = []

        if is_gt:
            for box in boxes:
                new_boxes.append([box[1], box[2], box[3], box[4], classes_dict[box[0]]])
        else:
            for box in boxes:
                if type(box[1]) == str:
                    new_boxes.append([box[2], box[3], box[4], box[5], classes_dict[box[1]], box[0]])
                else:
                    new_boxes.append([box[3], box[4], box[5], box[6], classes_dict[box[2]], box[0]])

        annotation_list.append([imgname, new_boxes])

    return annotation_list

if __name__=="__main__":
    classes = ('person', 'uniform')
    classes_dict = dict(zip(classes, range(len(classes))))
    anno_gt = 'pred_resize/labels'
    anno_pred = 'pred'

    ann_list_gt = convert_dir_to_list(anno_gt, classes_dict)
    ann_list_pred = convert_dir_to_list(anno_pred, classes_dict, is_gt=False)

    mAP_threshold = (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8)
    min_confidences = (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95)
    image_folder = 'res'
    plot_graphics(ann_list_gt, ann_list_pred, classes, mAP_threshold, min_confidences, image_folder)
