# Active learning comparison 

Тестирование метода Learning Loss for Actove Learning [Yoo et al. 2019 CVPR] для обнаружения объектов на наборе данных Kitti


## Результаты
Active learning – изображения в цикле сортируются по возрастанию выхода доп. нейросети,  
Active learning reverse – по убыванию

![plot](./al_plot.png)

## Данные
Используется данные для kitti 2d object detection (7481 изображений).  
[Скачать](https://drive.google.com/file/d/15hY0ReNJgatQ-JI2TNqyCJYdF0-ECOxY/view?usp=sharing) аннотации в формате VOC  
[Скачать](https://drive.google.com/drive/folders/18h7s34nocY7UrteOgcv3THfRn48Fh9d8?usp=sharing) текстовые файлы разбиения на подвыборки (70, 10, 20 - train, val, test) и конфиг для нейросети

[Скачать](https://drive.google.com/drive/folders/1ePyJcE7hMK-EgEhqitBMIPxz4HJQouU-?usp=sharing) результатат одного эксперимента (метрики, веса, галлереи изображений)

Файловая структура для использования указанных текстовых файлов (базовая директория указывается в конфиге).

```
kitti
└── training
    └── Annotations
            ...
            ...
    └── image_2
            ...
            ...
```



## Установка и запуск

Эксперимент проводился на ubuntu 16.04, python 3.6, CUDA 10.0 с использованием pytorch==1.4.0, torchvision==0.5.0, зависимости указаны в `requirements.txt`.

Перед использованием необходимо собрать deformable convolutions (не собирается с GPU support из докера =( ):  
`cd /centernet-active-learning/src/lib/models/networks/`  
`git clone --recursive https://github.com/CharlesShang/DCNv2`  
`cd DCNv2 && bash ./make.sh`  
А также собрать NMS  
`cd /centernet-active-learning/src/lib/external/`  
`make`   

запуск обучения:  
`python3 train.py path/to/config/file`

## Использованные ресурсы:
Основано на проекте Centerntet  
https://github.com/xingyizhou/CenterNet  
С использованием неофицальной имплементации learning Loss for Active learning  
https://github.com/Mephisto405/Learning-Loss-for-Active-Learning

## Презентация
[Презентация](https://drive.google.com/file/d/1bQr0f8kcdJoAdETrdR-FfbxpUeEgzFAl/view?usp=sharing) с описанием экспериментов и выводами. 

